#Author: Martin Becker, All Rights Reserved
#Licence: 3-clause BSD
require 'digest/sha1'
require 'erb'

namespace :choco do
	build_test = true
	desc 'build chocolatey package'
	task :build => [:templates, :package, :deploy]

	def get_root shifted_by,path_addition
		@p ||= File.expand_path(__FILE__).split('/')
		@p.shift(@p.length - shifted_by).join('/') +  path_addition || ''
	end 
	platform = { x86: 'windows-x86.7z', x64: 'windows-x86.7z' }

	archive = if URU_OPTS[:devbuild]
                      "uru-#{VER}-#{cs}-windows-"
                    else
                      "uru-#{VER}-windows-"
                    end

	choco_root = get_root(2, '/chocolatey/')
	template_root = get_root(1,'/templates/')

	@template  = {
		name: 'uru',
		platform: platform,
		ver: VER,
		chksm:  (build_test ? 'asdsadsadsad' : Digest::SHA1.file("#{PKG}/#{archive}#{platform[:x86]}").hexdigest),
		chksm64:(build_test ? 'qwewqrwqrwqr' : Digest::SHA1.file("#{PKG}/#{archive}#{platform[:x64]}").hexdigest),
		chksm_typ: 'sha1',
		authors: ['jonforums'],
		owners: ['jonforums'],
		prg_url: 'https://bitbucket.org/jonforums/',
		dependencies: ['7zip.commandline']		
	}

	templates = %w( uru.nuspec.erb tools/chocolateyinstall.ps1.erb )

	task :templates do
		templates.each do |template|
			File.open (choco_root + template.gsub('.erb','')) ,'w+' do |f|
				f.write(ERB.new(File.read(template_root + template)).result)
			end
		end
	end

	task :package do
		if system "cd #{choco_root} && cpack"
			puts 'Chocolatey package build successfully completed'
		else
			puts 'Chocolatey package build failed'
		end
	end

	task :deploy do
		 system "cd #{choco_root} && uru.#{@template[:ver]}.nupkg " unless build_test
	end
end